from setuptools import setup, find_packages


setup(
    name='analysis_tools',
    version='0.1.0',
    packages=find_packages(include=['analysis_tools', 'analysis_tools.*']),
    install_requires=['tqdm']
)
